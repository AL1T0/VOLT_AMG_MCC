library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

-- Definicion entradas y salidas del Contador Horizontal VGA de 10 bits
entity LOGICA is
	port(
		PX_X, PX_Y: in std_logic_vector(9 downto 0);
		ROM_COL, ROM_FIL: out std_logic_vector(2 downto 0);
		MUX_ENA: out std_logic;
        MUX_SEL: out std_logic_vector(2 downto 0)
		);   
end;

-- Arquitectura del contador Horizontal VGA
architecture arq_logica of LOGICA is
	signal pixel_y, pixel_x: std_logic_vector(9 downto 0);
	signal logica_mux_sel, logica_rom_col, logica_rom_fil, logica_mux_ena: std_logic_vector(2 downto 0);
begin
    --Asociacion de las señales internas con las entradas
    pixel_x <= PX_X;
    pixel_y <= PX_Y;

    --Decodificación del CH y CV para la ROM y el MUX
    logica_mux_sel   <= pixel_x(9 downto 7);
    logica_rom_col   <= pixel_x(6 downto 4);
    logica_mux_ena   <= pixel_y(9 downto 7);
    logica_rom_fil   <= pixel_y(6 downto 4);

    --Salida de la lógica
    ROM_COL <= logica_rom_col;
    ROM_FIL <= logica_rom_fil;
    MUX_ENA <= ((not logica_mux_ena(2)) and (not logica_mux_ena(1)) and logica_mux_ena(0));
    MUX_SEL <= logica_mux_sel;

end;
