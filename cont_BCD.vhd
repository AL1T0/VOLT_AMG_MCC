library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use work.pack.vector;

-- Contador BCD de 4 decadas
entity cont_BCD is
	-- Definición de E/S
	port(
		CLK, ENA, RST: in std_logic;
		-- EN: enable
		-- CLK: clock
		-- RST: reset
		Qu, Qd, Qc: out std_logic_vector(3 downto 0));
		-- Q: 3 palabras, de 4 bits cada una
end cont_BCD;

-- Arquitectura del contador BCD 4 de decadas
architecture arq_cont_BCD of cont_BCD is
	
	-- Señales Auxiliares
	signal Co, ENAa: std_logic_vector (4 downto 0);
	signal COUNT0, COUNT1, COUNT2, COUNT3, COUNT4: std_logic_vector (3 downto 0);
begin
    --Mapeo el contador BCD
	BCD0: entity work.BCD port map(CLK, ENAa(0), RST, COUNT0, Co(0));
	BCD1: entity work.BCD port map(CLK, ENAa(1), RST, COUNT1, Co(1));
	BCD2: entity work.BCD port map(CLK, ENAa(2), RST, COUNT2, Co(2)); --Centena
	BCD3: entity work.BCD port map(CLK, ENAa(3), RST, COUNT3, Co(3)); --Decena
	BCD4: entity work.BCD port map(CLK, ENAa(4), RST, COUNT4, Co(4)); --Unidad
	
	--Genero las habilitaciones en cascada 
	ENAa(0) <= ENA;
	ENAa(1) <= ENAa(0) and COUNT0(0) and not COUNT0(1) and not COUNT0(2) and COUNT0(3);--Co(0) and ENAa(0);
	ENAa(2) <= ENAa(1) and COUNT1(0) and not COUNT1(1) and not COUNT1(2) and COUNT1(3);
	ENAa(3) <= ENAa(2) and COUNT2(0) and not COUNT2(1) and not COUNT2(2) and COUNT2(3);
	ENAa(4) <= ENAa(3) and COUNT3(0) and not COUNT3(1) and not COUNT3(2) and COUNT3(3);
	
	--Asigno el conteo a la salida
	Qu <= COUNT4;
	Qd <= COUNT3; 
	Qc <= COUNT2;
	
end arq_cont_BCD;

--Test bench
-- library IEEE;
-- use IEEE.std_logic_1164.all;       
-- use work.pack.vector;

-- entity test_cont_BCD is
-- end;

-- architecture arq_t_cont_BCD of test_cont_BCD is
--   component cont_BCD is
--     port(
-- 		CLK, ENA, RST: in std_logic;
-- 		Q: out vector(3 downto 0) );
--   end component;
  
-- signal tENA, tCLK, tRST: std_logic:='0';
-- signal tQ: vector (3 downto 0);

-- begin
--   tENA <= '0', '1' after 25 ns;
--   tRST <= '1', '0' after 15 ns;
--   tCLK <= not tCLK after 10 ns;
  
--   CONT4DEC: cont_BCD port map(tCLK, tENA, tRST, tQ);

-- end arq_t_cont_BCD;