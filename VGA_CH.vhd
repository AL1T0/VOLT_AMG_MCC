library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

-- Definicion entradas y salidas del Contador Horizontal VGA de 10 bits
entity VGA_CH is
	port(
		CLK, ENA: in std_logic;
		Q1: out std_logic_vector(9 downto 0);
		Q2: out std_logic
		);   
end;

-- Arquitectura del contador Horizontal VGA
architecture arq_vga_ch of VGA_CH is
	signal Qa, Ta: std_logic_vector(9 downto 0):="0000000000";
	signal RST1, RST2, Q: std_logic:='0';

begin
	-- Genero los 9 FFT para el conteo
	genfft: for i in 0 to 9 generate
	FF_Tn: entity work.FF_T port map(CLK=>CLK, ENA=>ENA, RST=>RST1, T=>Ta(i), Q=>Qa(i));
	end generate;
	
	-- CLK, ENA, RST, D
	FF_d0: entity work.FF_D port map(CLK=>CLK, ENA=>'1', RST=>'0', D=>RST2, Q=>Q);

	-- Genero la secuencia de conteo
	Ta(0) <= '1';
    Ta(1) <= Qa(0);
	gen_cnt: for j in 1 to 8 generate
	Ta(j+1) <= Ta(j) and Qa(j);
	end generate;
    
	RST1 <= (Qa(9) and Qa(8) and (not (Qa(7))) and (not(Qa(6))) and Qa(5) and (not(Qa(4))) and (not(Qa(3))) and (not(Qa(2))) and (not(Qa(1))) and not(Qa(0)));
	--se resetea el bloque cuando la cuenta llega a 800
	RST2 <= (Qa(9) and Qa(8) and (not (Qa(7))) and (not(Qa(6))) and (not(Qa(5))) and (Qa(4)) and (Qa(3)) and (Qa(2)) and (Qa(1)) and (Qa(0)));
	--Cuando la cuenta llega a 799 se incrementa en 1 el contador horizontal
	Q1 <= Qa;
	Q2 <= Q;
end;
