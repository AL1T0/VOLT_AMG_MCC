library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

-- Contador binario de 0 a 33000
entity cont_N is
	port(
		CLK: in std_logic;
		OUT1, OUT2: out std_logic; 
		Q: out std_logic_vector(15 downto 0));
end cont_N;

-- Arquitectura del contador binario
architecture arq_cont_N of cont_N is
	signal Qa, Ta: std_logic_vector(15 downto 0);
	signal RST: std_logic;
begin
	
	-- Genero los 15 FFT para el conteo
	genfft: for i in 0 to 15 generate
	FFTn: entity work.FF_T port map(CLK=>CLK, ENA=>'1', RST=>RST, T=>Ta(i), Q=>Qa(i));
	end generate;
	
	-- Genero la secuencia de conteo
	Ta(0) <= '1';
	Ta(1) <= Qa(0);
	gencnt_ta: for j in 1 to 14 generate
	Ta(j+1) <= Ta(j) and Qa(j);
	end generate;

	OUT1 <= (not(Qa(0)) and not(Qa(1)) and not(Qa(2)) and Qa(3) and not(Qa(4)) and Qa(5) and Qa(6) and Qa(7) and not(Qa(8)) and not(Qa(9)) and not(Qa(10)) and not(Qa(11)) and not(Qa(12)) and not(Qa(13)) and not(Qa(14)) and Qa(15)); -- se activa cuando llega a 33000 (registro conteo)
	RST  <= (Qa(0)  	and not(Qa(1)) and not(Qa(2)) and Qa(3) and not(Qa(4)) and Qa(5) and Qa(6) and Qa(7) and not(Qa(8)) and not(Qa(9)) and not(Qa(10)) and not(Qa(11)) and not(Qa(12)) and not(Qa(13)) and not(Qa(14)) and Qa(15)); -- se activa cuando llega a 33001 (reseteo los FFT)
	OUT2 <= RST;
	-- Para prueba
	Q <= Qa;
end arq_cont_N;


-- -- Test bench
-- library IEEE;
-- use IEEE.std_logic_1164.all;       

-- entity test_cont_N is
-- end test_cont_N;

-- architecture arq_t_cont_N of test_cont_N is
  
-- signal tCLK, tOUT1, tOUT2 : std_logic := '0'; 
-- signal tQ : std_logic_vector(15 downto 0);

-- begin
-- 	tCLK <= not tCLK after 10 ns;
-- 	CONTADOR: entity work.cont_N port map(CLK=>tCLK, OUT1=>tOUT1, OUT2=>tOUT2, Q=>tQ); 
-- end arq_t_cont_N;