library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity FF_D is
	port( 
		CLK, ENA, RST, D: in std_logic;
		-- CLK: clock
		-- ENA: enable 
		-- RST: clear
		-- D: data
		Q, nQ: out std_logic); 
		-- Q: salida
		-- nQ: salida negada
end FF_D;

architecture arq_ff_d of FF_D is
begin
	process(CLK, ENA, RST)
		begin
		if RST = '1' then 
		  Q <= '0' ;
		  nQ <= '1';
		elsif rising_edge(CLK) and ENA = '1' then 
		  Q <= D ;
		  nQ <= not D;
		end if;
	end process;
end arq_ff_d;


-- Test bench
-- library IEEE;
-- use IEEE.std_logic_1164.all; 
-- use IEEE.STD_LOGIC_ARITH.ALL;
-- use IEEE.STD_LOGIC_UNSIGNED.ALL;
 
-- entity test_FFD is
-- end test_FFD;

-- architecture arq_t_FFD of test_FFD is
-- 	component FF_D
-- 		port(
-- 			CLK, ENA, RST, D: in std_logic;
-- 			Q, nQ: out std_logic);
-- 	end component;

-- 	signal tCLK : std_logic := '0';
-- 	signal tENA : std_logic := '1';
-- 	signal tRST : std_logic := '1';
-- 	signal tD : std_logic := '0';
-- 	signal tQ : std_logic := '0';
-- 	signal tnQ : std_logic := '0';

-- begin
-- 	FFD: FF_D port map(CLK=>tCLK, ENA=>tENA, RST=>tRST, D=>tD, Q=>tQ, nQ=>tnQ);
-- 	tRST <= not tRST after 1000 ns; 
-- 	tENA <= not tENA after 500 ns;   
-- 	tCLK <= not tCLK after 10 ns;
-- 	tD <= not tD after 50 ns; 
-- end arq_t_FFD;