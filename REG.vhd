library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
--use work.utils.vector;

entity REG is
	port(
		CLK, ENA: in std_logic;
		Du, Dd, Dc: in std_logic_vector(3 downto 0);
		Qu, Qd, Qc: out std_logic_vector(3 downto 0));
end REG;

--Arquitectura del Registro
architecture arq_REG of REG is
	signal Qa, Da: std_logic_vector(11 downto 0);
begin
	-- Genero los 16 FFD
	genffd: for i in 11 downto 0 generate
	FFDn: entity work.FF_D port map(CLK=>CLK, ENA=>ENA, RST=>'0', D=>Da(i), Q=>Qa(i));
	end generate;
		
	Da(3 downto 0) <= Du;
	Da(7 downto 4) <= Dd;
	Da(11 downto 8)<= Dc;
	
	Qu <= Qa(3 downto 0);
	Qd <= Qa(7 downto 4);
	Qc <= Qa(11 downto 8);
end arq_REG;
