library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

entity MUX is 
      port(
        ENA : in  std_logic;                     --Habilitacion del MUX si estoy en la parte visible de la pantalla      
        SEL : in  std_logic_vector (2 downto 0); --Selector
        A0, A1, A2, A3, A4: in  std_logic_vector (3 downto 0);   	
          --A0 1º digito BCD
          --A1 Caracter "."
          --A2 2º digito BCD
          --A3 3º digito BCD
          --A4 Caracter "V"
        Q: out std_logic_vector (3 downto 0) --Salida a ROM/VGA
        );  
      end ;
   
architecture arq_mux of MUX is
     
    signal unidad, decena, centena, punto, volt, blank: std_logic_vector (3 downto 0);
    signal uni, dec, cent, pto, v, fn: std_logic_vector (3 downto 0);
    signal SELa :std_logic_vector (2 downto 0);
    signal ENAa :std_logic;
    
    begin
     
        --Señales asociadas para seleccion       
        unidad      <= ( 3 downto 0 =>((not SELa(2)) and (not  SELa(1)) and (not SELa(0)) and ENAa)); 
		punto    	<= ( 3 downto 0 =>((not SELa(2)) and (not  SELa(1)) and (    SELa(0)) and ENAa));
        decena  	<= ( 3 downto 0 =>((not SELa(2)) and (     SELa(1)) and (not SELa(0)) and ENAa));
        centena     <= ( 3 downto 0 =>((not SELa(2)) and (     SELa(1)) and (    SELa(0)) and ENAa));
  		volt 	    <= ( 3 downto 0 =>((    SELa(2)) and (not  SELa(1)) and (not SELa(0)) and ENAa));
        blank   <= not(( 3 downto 0 =>((not SELa(2)) and (not  SELa(1)) and (not SELa(0)) and ENAa)) 
		or   	       ( 3 downto 0 =>((not SELa(2)) and (not  SELa(1)) and (    SELa(0)) and ENAa))
		or   	       ( 3 downto 0 =>((not SELa(2)) and (     SELa(1)) and (not SELa(0)) and ENAa))
 		or   	       ( 3 downto 0 =>((not SELa(2)) and (     SELa(1)) and (    SELa(0)) and ENAa))
		or   	       ( 3 downto 0 =>((    SELa(2)) and (not  SELa(1)) and (not SELa(0)) and ENAa)));

		--Salida del mux
        Q      <= (unidad and uni) or (centena and cent) or (decena and dec) or (punto and pto) or (volt and v) or (blank and fn);
        SELa   <= SEL;
        ENAa   <= ENA;
                
		 --Asociacion con el vector de entradas desde el contador BCD 
        uni  <= A0;		
        pto  <= A1;		
        dec  <= A2;		
        cent <= A3;	
        v    <= A4;
        fn   <= "1111";
end;

-- --Test bench
-- library IEEE;
-- use IEEE.std_logic_1164.all;
-- use IEEE.std_logic_unsigned.all;

-- entity test_mux is 
-- end;

-- architecture arq_test_mux of test_mux is

--     signal ena: std_logic_vector (4 downto 0);
--     signal sel: std_logic_vector (5 downto 0);
--     signal A0: std_logic_vector (3 downto 0);    		--1 digito BCD
--     signal A1: std_logic_vector (3 downto 0);    		--Caracter "punto"
--     signal A2: std_logic_vector (3 downto 0);    		--2 digito BCD
--     signal A3: std_logic_vector (3 downto 0);    		--3 digito BCD
--     signal A4: std_logic_vector (3 downto 0);    		--4 digito BCD
--     signal A5: std_logic_vector (3 downto 0);    		--Letra "V"
--     signal A6: std_logic_vector (3 downto 0);    		--Blank
--     signal Q: std_logic_vector (3 downto 0); 
   
-- begin 
-- 	inst : entity work.Mux port map (ena, sel, A0, A1, A2, A3, A4, A5, A6, Q);
-- 	A0 <= "0000"; -- Valores arbitrarios
-- 	A1 <= "0001";
-- 	A2 <= "0010";
-- 	A3 <= "0111";
-- 	A4 <= "1100";
-- 	A5 <= "1101";
-- 	A6 <= "1010";
-- 	ena <= "11110";
-- 	sel <= "101010"; 
-- -- 101001 Unidad de mil (A0)
-- -- 100110 Punto (A1)
-- -- 100111 Centena (A2)
-- -- 101000 Decena (A3)
-- -- 101001 Unidad (A4)
-- -- 101010 Caracter (A5)
-- -- 000000 Blank (A6)
-- end;
