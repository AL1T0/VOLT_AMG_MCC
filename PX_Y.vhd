library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
 
entity PX_Y is 
      port(
          SEL : in std_logic;    			       --Seleccionador
          VC: in std_logic_vector (9 downto 0);    --vc
          QY: out std_logic_vector (9 downto 0)     --Salida a pixel_y
          );  
      end ;
   
architecture arq_px_y of PX_Y is
    signal SELa : std_logic;
    signal s1, s2, VC1a, VC2a, aux: std_logic_vector (9 downto 0);
    
    begin
		--Asociacion con el vector de entradas.
        VC1a <= VC;
        VC2a <= VC-31;        
        SELa <= SEL;

        --Señales asociadas para selection.
        s1 <= (9 downto 0 => SELa);
        s2 <= (9 downto 0 =>(not SELa));
       
       --salida del multiplexor
        QY <= (VC2a and s1) or (VC1a and s2);
end ;


-- Test bench
-- library IEEE;
-- use IEEE.std_logic_1164.all;
-- use IEEE.std_logic_unsigned.all;

-- entity test_mux_x is 
-- end;

-- architecture arq_test_mux_x of test_mux_x is
-- 	signal vc1: std_logic_vector (9 downto 0);
-- 	signal vc2: std_logic_vector (9 downto 0);
-- 	signal Q: std_logic_vector (9 downto 0);    
-- 	signal sel: std_logic;
-- begin 

-- 	inst : entity work.mux_x port map (sel, vc1, vc2, Q);
-- 	sel <= '0';
-- 	vc1 <= "1111111111";
-- 	vc2 <="1111111111";
	
-- end;
