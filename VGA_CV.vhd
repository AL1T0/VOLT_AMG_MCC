library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

-- Definicion entradas y salidas del Contador Vertical VGA de 10 bits
entity VGA_CV is
	port(
		CLK, ENA: in std_logic;
		Q  : out std_logic_vector(9 downto 0)
		);   
end;

-- Arquitectura del contador Vertical VGA
architecture arq_vga_cv of VGA_CV is
	signal Qa, Ta: std_logic_vector(9 downto 0);
	signal RST: std_logic;
begin
	-- Genero los 9 FFT para el conteo
	gen_fft: for i in 0 to 9 generate
	FF_Tnn: entity work.FF_T port map(CLK=>CLK, ENA=>ENA, RST=>RST, T=>Ta(i), Q=>Qa(i));
	end generate;
	
	-- Genero la secuencia de conteo
	Ta(0) <= '1';
    Ta(1) <= Qa(0);
	gen_cntt: for j in 1 to 8 generate
	Ta(j+1) <= Ta(j) and Qa(j);
	end generate;
    
	RST <= (Qa(9) and (not(Qa(8))) and (not (Qa(7))) and (not(Qa(6))) and (not(Qa(5))) and (not(Qa(4))) and (Qa(3)) and (not(Qa(2))) and (not(Qa(1))) and (Qa(0)));
	-- se resetea el bloque cuando la cuenta llega a 521
	Q <= Qa;
end;