library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
 
entity PX_X is 
      port(
          SEL : in std_logic;    			       --Seleccionador
          HC: in std_logic_vector (9 downto 0);    --hc
          QX: out std_logic_vector (9 downto 0)    --Salida a pixel_x
          );  
      end ;
   
architecture arq_px_x of PX_X is
    
    signal SELa : std_logic;
    signal s1, s2, HC1a, HC2a, aux: std_logic_vector (9 downto 0);
    
    begin
        --Asociacion de las señales internas con el vector de entradas.
        HC1a <= HC;
        HC2a <= HC-144;      
        SELa <= SEL;
       
       --Señales asociadas para seleccion
        s1 <= (9 downto 0 => SELa);
        s2 <= (9 downto 0 =>(not SELa));

       --salida del multiplexor
        QX <= (HC2a and s1) or (HC1a and s2);
end ;

-- Test bench
-- library IEEE;
-- use IEEE.std_logic_1164.all;
-- use IEEE.std_logic_unsigned.all;

-- entity test_mux_y is 
-- end;

-- architecture arq_test_mux_y of test_mux_y is

-- 	signal hc1: std_logic_vector (9 downto 0);
-- 	signal hc2: std_logic_vector (9 downto 0);
-- 	signal Q: std_logic_vector (9 downto 0);    
-- 	signal sel: std_logic;
   
-- begin 

-- 	inst : entity work.mux_y port map (sel, hc1, hc2, Q);
-- 	sel <= '0';
-- 	hc1 <= "1011100010";
-- 	hc2 <="1011100001";
	
-- end;