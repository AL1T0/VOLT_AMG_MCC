library IEEE;
use IEEE.std_logic_1164.all;

--Contador BCD que cuenta de 0 a 9
entity BCD is
	-- Definicion de E/S
	port(
		CLK, ENA, RST : in std_logic;
		COUNT: out std_logic_vector(3 downto 0);
		-- COUNT(0): MSB
		-- COUNT(4): LSB
		
		Co: out std_logic);
		-- Co: CARRY OUT
end BCD;

--Arquitectura del contador BCD
architecture arq_bcd of BCD is 
	-- Señales Auxiliares
	signal Da, Qa, nQa: std_logic_vector(3 downto 0) := "0000";

begin
	-- Mapeo el FF_D
	ff_d0: entity work.ff_d port map(CLK, ENA, RST, Da(0), Qa(0), nQa(0));
	ff_d1: entity work.ff_d port map(CLK, ENA, RST, Da(1), Qa(1), nQa(1));
	ff_d2: entity work.ff_d port map(CLK, ENA, RST, Da(2), Qa(2), nQa(2));
	ff_d3: entity work.ff_d port map(CLK, ENA, RST, Da(3), Qa(3), nQa(3));

	Da(0) <=  nQa(0);
	Da(1) <= (nQa(0) and Qa(1)) or (Qa(0) and nQa(1) and nQa(3));
	Da(2) <= (nQa(0) and Qa(2)) or (Qa(0) and Qa(1)  and nQa(2)) or (Qa(0) and nQa(1) and Qa(2));
	Da(3) <= (nQa(0) and Qa(3)) or (Qa(0) and Qa(1)  and Qa(2));

	-- Asigno la cuenta al puerto de salida del contador
	COUNT <= Qa;
	Co <= Qa(0) and Qa(3);
	
end arq_bcd; 

--Test bench
-- library IEEE;
-- use IEEE.std_logic_1164.all;       

-- entity test_BCD is
-- end;

-- architecture arq_t_BCD of test_BCD is
-- 	component BCD is
-- 		port(
-- 			CLK, ENA, RST : in std_logic;
-- 			COUNT: out std_logic_vector(3 downto 0);
-- 			Co : out std_logic);
-- 	end component;
  
-- 	signal tCLK, tENA, tRST: std_logic := '0';
-- 	signal tCOUNT: std_logic_vector(3 downto 0) := "0000";
-- 	signal tCo: std_logic := '0';

-- 	begin
-- 		tENA <= '0', '1' after 25 ns;
-- 		tRST <= '1', '0' after 15 ns;
-- 		tCLK <= not tCLK after 10 ns;
-- 		CONT_BCD: BCD port map(tCLK, tENA, tRST, tCOUNT, tCo);
-- end arq_t_BCD;
