library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;


entity VGA is
	port (
		CLK: in std_logic;	-- reloj del sistema (50 MHz)
        R_I: in std_logic;	-- entrada comandada por uno de los switches del kit
        G_I: in std_logic;	-- entrada comandada por uno de los switches del kit
        B_I: in std_logic;	-- entrada comandada por uno de los switches del kit
        SH: out std_logic;	-- sincronismo horizontal
        SV: out std_logic;	-- sincronismo vertical
        R_O: out std_logic;	-- salida de color rojo	
        G_O: out std_logic;	-- salida de color verde
        B_O: out std_logic;	-- salida de color azul
		PIXEL_X: out std_logic_vector(9 downto 0);	--	posición horizontal del pixel en la pantalla
		PIXEL_Y: out std_logic_vector(9 downto 0);	--	posición vertical del pixel en la pantalla
		HAB_REG2: out std_logic	-- salida de habilitació al registro anti glitch
	);
end;

architecture arq_VGA of VGA is

	constant hpixels: std_logic_vector(9 downto 0):="1100100000";	-- Número de pixeles en una linea horizontal (800, "1100100000")
	constant vlines: std_logic_vector(9 downto 0):= "1000001001";	-- Número de lineas horizontales en el display (521, "1000001001")
	
	constant hpw: natural:= 97; 									-- Ancho del pulso de sincronismo horizontal [pixeles]
	constant hbp: std_logic_vector(9 downto 0):= "0010010000";      -- Back porch horizontal (144, "0010010000")
	constant hfp: std_logic_vector(9 downto 0):= "1100010000";	    -- Front porch horizontal (784, "1100010000")

	constant vpw: natural:= 3; 									-- Ancho del pulso de sincronismo vertical [líneas]
	constant vbp: std_logic_vector(9 downto 0):= "0000011111";	 	-- Back porch vertical (31, "0000011111")
	constant vfp: std_logic_vector(9 downto 0):= "0111111111";		-- Front porch vertical (511, "0111111111")
	
	signal hc, vc: std_logic_vector(9 downto 0);					-- Contadores (horizontal y vertical)
	signal clkdiv_flag: std_logic;      							-- Flag para obtener una habilitación cada dos ciclos de clock
	signal vidon: std_logic;										-- Habilita la visualización de datos
	signal hab_SV, hab_CV: std_logic;					-- Habilita el contador vertical
	
begin
    -- Generación de la señal de habilitación para dividir el clock de 50Mhz a la mitad
	fft25MHz: entity work.FF_T port map(CLK=>CLK, ENA=>'1', RST=>'0', T=>'1', Q=>clkdiv_flag);

    -- Contador para el sincronismo horizontal
    CH_VGA: entity work.VGA_CH port map(CLK=>CLK, ENA=>clkdiv_flag, Q1=>hc, Q2=>hab_SV);
   
    -- Contador para el sincronismo vertical
	hab_CV<= clkdiv_flag and hab_SV;    
	CV_VGA: entity work.VGA_CV port map(CLK=>CLK, ENA=>hab_CV, Q=>vc);
	
	-- Generación de señales de sincronismo horizontal y vertical
    SH <= '1' when (hc < hpw) else '0';
    SV <= '1' when (vc < vpw) else '0';
	
	--Ubicación relativa dentro de la parte visible de la pantalla
	--Pixel_Y
	px_y: entity work.PX_Y port map(SEL=>vidon, VC=>vc, QY=>PIXEL_Y);
	
	--Pixel_X
	px_x: entity work.PX_X port map(SEL=>vidon, HC=>hc, QX=>PIXEL_X);
	
	-- Señal de habilitación para la salida de datos por el display, se pone en 1 cuando me encuentro en la parte visible del display
	vidon <= '1' when (((hfp > hc) and (hc > hbp)) and ((vfp > vc) and (vc > vbp))) else '0';
	HAB_REG2 <= '1' when (hc = "0000000000" and vc = "0000000000") else '0';

    R_O <= '1' when (R_I = '1' and vidon = '1') else '0';	-- Pinta la pantalla del color formado
    G_O <= '1' when (G_I = '1' and vidon = '1') else '0';	-- por la combinación de las entradas
    B_O <= '1' when (B_I = '1' and vidon = '1') else '0';	-- R_I, G_I y B_I

end;

-----------
-- library IEEE;
-- use IEEE.std_logic_1164.all;

-- entity test_VGA is
-- end;

-- architecture test_VGA_arq of test_VGA is
-- 	signal clk: std_logic := '0';
-- 	signal r_i, g_i, b_i: std_logic := '1';
-- 	signal sh, sv: std_logic;
-- 	signal r_o, g_o, b_o: std_logic;
-- 	signal pixel_x, pixel_y: std_logic_vector(9 downto 0);

-- begin
-- 	clk <= not clk after 20 ns;
-- 	aaa: entity work.VGA
-- 		port map(
-- 		CLK => clk,
--         R_I => r_i,
--         G_I => g_i,
--         B_I => b_i,
--         SH => sh,
--         SV => sv,
--         R_O => r_o,
--         G_O => g_o,
--         B_O => b_o,
-- 		PIXEL_X => pixel_x,
-- 		PIXEL_Y => pixel_y
-- 		);
-- end; 