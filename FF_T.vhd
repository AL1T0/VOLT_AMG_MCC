library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity FF_T is
   port( 
		CLK, ENA, RST, T: in std_logic;
		-- ENA: enable
		-- CLK: clock 
		-- RST: clear
		-- T: toggle
		Q, nQ: out std_logic);
		-- Q: salida
		-- nQ: salida negada
end FF_T;
 
architecture arq_FF_T of FF_T is
	signal AUX: std_logic:='0';
begin
	process(CLK, ENA, RST, T)
		begin
		if RST = '1' then
			AUX <= '0';     
		elsif rising_edge (CLK) and ENA = '1' then
			if  T = '1' then
				AUX <= not AUX;    
			end if;
		end if;
	end process;
    Q <= AUX;
    nQ <= not AUX;
end arq_FF_T;


-- Test bench
-- library IEEE;
-- use IEEE.std_logic_1164.all; 
-- use IEEE.STD_LOGIC_ARITH.ALL;
-- use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- entity test_FFT is
-- end test_FFT;

-- architecture arq_t_FFT of test_FFT is
-- 	component FF_T
-- 		port(
-- 			CLK, ENA, RST, T: in std_logic;
-- 			Q, nQ: out std_logic);
-- 	end component;

-- 	signal tCLK : std_logic := '0';
-- 	signal tENA : std_logic := '1'; 
-- 	signal tRST : std_logic := '1';
-- 	signal tT : std_logic := '0';
-- 	signal tQ : std_logic := '0';
-- 	signal tnQ : std_logic := '0';

-- begin
--     FFT: FF_T port map(CLK=>tCLK, ENA=>tENA, RST=>tRST, T=>tT, Q=>tQ, nQ=>tnQ);
--     tRST <= not tRST after 1000 ns; 
--     tENA <= not tENA after 500 ns;   
--     tCLK <= not tCLK after 10 ns;
--     tT <= not tT after 50 ns;
-- end arq_t_FFT;