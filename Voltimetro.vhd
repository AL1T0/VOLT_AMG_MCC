library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use work.pack.all;

-- Definicion entradas y salidas del voltimetro
entity Voltimetro is
	port(
	    -- pinD5: out std_logic;-- Salida negadada del ADC
		-- pinD6: in std_logic; -- Entrada de tensión
		data_volt_in_p: in std_logic;
		data_volt_in_n: in std_logic;
		data_volt_out: out std_logic;
		
		RST: in std_logic ;
		CLK: in std_logic ;
		SH: out std_logic;	-- sincronismo horizontal
        SV: out std_logic;	-- sincronismo vertical
        R_O: out std_logic;	-- salida de color rojo	
        G_O: out std_logic;	-- salida de color verde
        B_O: out std_logic	-- salida de color azul		
      );
    
	-- attribute LOC: string;
	-- attribute LOC of pinD5: signal is "B4";    --FOXIO9
	-- attribute LOC of pinD6: signal is "A4";    --FFOXIO10
	-- attribute LOC of RST: 	signal is "D18";   --Boton "Bin West"
	-- attribute LOC of CLK: 	signal is "C9";
	-- attribute LOC of SH: 	signal is "F15";
	-- attribute LOC of SV: 	signal is "F14";
	-- attribute LOC of R_O:	signal is "H14";
	-- attribute LOC of G_O: 	signal is "H15";
	-- attribute LOC of B_O: 	signal is "G15";
	
	attribute loc: string;
	attribute slew: string;
	attribute drive: string;
	attribute iostandard: string;	-- attribute LOC of pinD5: signal is "B4";    --FOXIO9
	-- attribute LOC of pinD6: signal is "A4";    --FFOXIO10
	attribute LOC of RST: 	signal is "L13";   --Boton "Bin West"
	attribute LOC of CLK: 	signal is "C9";
	
	-- Entradas diferenciales
	attribute iostandard of data_volt_in_p: signal is "LVDS_25";	
	attribute loc of data_volt_in_p: signal is "A4";
	attribute iostandard of data_volt_in_n: signal is "LVDS_25";	
	attribute loc of data_volt_in_n: signal is "B4";

	-- Salida realimentada
	attribute loc of data_volt_out: signal is "C5";
	attribute slew of data_volt_out: signal is "FAST";
	attribute drive of data_volt_out: signal is "8";
	attribute iostandard of data_volt_out: signal is "LVCMOS25";
	
	attribute LOC of SH: 	signal is "F15";
	attribute LOC of SV: 	signal is "F14";
	attribute LOC of R_O:	signal is "H14";
	attribute LOC of G_O: 	signal is "H15";
	attribute LOC of B_O: 	signal is "G15";
	
end;

architecture arq_voltimetro of Voltimetro is
	signal bcd_reg_0: std_logic_vector (3 downto 0);--conecta cont_BCD a reg
	signal bcd_reg_1: std_logic_vector (3 downto 0);--conecta cont_BCD a reg
	signal bcd_reg_2: std_logic_vector (3 downto 0);--conecta cont_BCD a reg
	signal adc_bcd: std_logic; --ADC con enable cont_BCD
	signal contn_bcd: std_logic; -- out2 con reset cont_BCD (señal que restea cont_BCD)--
	signal contn_reg: std_logic; -- out1 con enable a registro (cuando guarda los datos)--
	signal reg_reg2_0: std_logic_vector (3 downto 0);--conecta reg a reg2
	signal reg_reg2_1: std_logic_vector (3 downto 0);--conecta reg a reg2
	signal reg_reg2_2: std_logic_vector (3 downto 0);--conecta reg a reg2
	signal reg_q0_mux:std_logic_vector (3 downto 0); -- salida de registro Q0 con entrada A0 del multiplexor--
	signal reg_q1_mux:std_logic_vector (3 downto 0); -- salida de registro Q1 con entrada A2 del multiplexor--
	signal reg_q2_mux:std_logic_vector (3 downto 0); -- salida de registro Q2 con entrada A3 del multiplexor--
	signal mux_rom:std_logic_vector (3 downto 0); --salida del multiplexor, entrada del roROMm--
	signal rom_vga: std_logic; -- salida de ROM a entrada VGA--
	signal logica_mux_sel:std_logic_vector (2 downto 0);
	signal logica_mux_ena:std_logic;
	signal logica_rom_col:std_logic_vector (2 downto 0);
	signal logica_rom_fil:std_logic_vector (2 downto 0);
	signal pixel_x:std_logic_vector (9 downto 0);
	signal pixel_y:std_logic_vector (9 downto 0);
	signal notQ: std_logic;
	signal hab_r2: std_logic;
	
	component IBUFDS 
		port(
			I : in std_logic; 
			IB : in std_logic; 
			O : out std_logic
		); 
	end component;
	
	signal Diff_Input: std_logic;
		
begin

	ibuf0: IBUFDS port map(
		I => data_volt_in_p,
		IB => data_volt_in_n,
		O => Diff_Input
	);
	
-- ADC
-- ADC: entity work.FF_D port map(CLK=>CLK, ENA=>'1', RST=>RST, D=>pinD6, Q=>adc_bcd, nQ=>notQ); --(CLK, enable, reset, D, Q, not Q)
--pinD5<=notQ;
ADC: entity work.FF_D port map(CLK=>CLK, ENA=>'1', RST=>RST, D=>Diff_Input, Q=>adc_bcd, nQ=>notQ); --(CLK, enable, reset, D, Q, not Q)
data_volt_out <= adc_bcd;

--BCD 4 decadas--
cont_BCD: entity work.cont_BCD port map(CLK=>CLK, ENA=>adc_bcd, RST=>contn_bcd, Qu=>bcd_reg_0, Qd=>bcd_reg_1, Qc=>bcd_reg_2); --(CLK, enable, reset, Q)

--Cont Nbits--
cont_N: entity work.cont_N port map(CLK=>CLK, OUT1=>contn_reg, OUT2=>contn_bcd); --(CLK, out1, out2)--

--Registro--
REG: entity work.REG port map(CLK=>CLK, ENA=>contn_reg, Du=>bcd_reg_0, Dd=>bcd_reg_1, Dc=>bcd_reg_2, Qu=>reg_reg2_0, Qd=>reg_reg2_1, Qc=>reg_reg2_2); --(CLK, ENA, D, Q)--
REG2: entity work.REG port map(CLK=>CLK, ENA=>hab_r2, Du=>reg_reg2_0, Dd=>reg_reg2_1, Dc=>reg_reg2_2, Qu=>reg_q0_mux, Qd=>reg_q1_mux, Qc=>reg_q2_mux); --(CLK, ENA, D, Q)--

--Lógica--
LOGICA: entity work.LOGICA port map(PX_X=>pixel_x, PX_Y=>pixel_y, ROM_COL=>logica_rom_col, ROM_FIL=>logica_rom_fil, MUX_ENA=>logica_mux_ena, MUX_SEL=>logica_mux_sel);

--Multiplexor--
MUX: entity work.MUX port map(ENA=>logica_mux_ena, SEL=>logica_mux_sel, A0=>reg_q0_mux, A1=>"1011", A2=>reg_q1_mux, A3=>reg_q2_mux, A4=>"1010",Q=>mux_rom); --

--Rom--
ROM: entity work.ROM port map(DIR_CARACTER=>mux_rom, FILA=>logica_rom_fil, COLUMNA=>logica_rom_col, Q=>rom_vga); --char_address--

-- Controlador VGA--
VGA: entity work.VGA port map(CLK=>CLK, R_I=>rom_vga, G_I=>rom_vga, B_I=>rom_vga, SH=>SH, SV=>SV, R_O=>R_O, G_O=>G_O, B_O=>B_O, PIXEL_X=>pixel_x, PIXEL_Y=>pixel_y, HAB_REG2=>hab_r2); 

end;